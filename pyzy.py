#! /bin/env python3
import os
import sys
import textwrap

import ocrmypdf
import textract
from sumy.nlp.stemmers import Stemmer
from sumy.nlp.tokenizers import Tokenizer
from sumy.parsers.plaintext import PlaintextParser
from sumy.summarizers.text_rank import TextRankSummarizer

# must be pdf
pdf = sys.argv[1]
sentence_count = sys.argv[2]

pdf_ocr = os.path.splitext(pdf)[0] + "_ocr.pdf"
summary_txt = os.path.splitext(pdf)[0] + "_summary.txt"
language = "english"

text = textract.process(pdf)
if "".join(str(text.split())) == "":
    if not os.path.isfile(pdf_ocr):
        ocrmypdf.ocr(pdf, pdf_ocr, deskew=True)
    text = textract.process(pdf_ocr, memothod="pdfminer")

stemmer = Stemmer(language)
tokenizer = Tokenizer(language)
parser = PlaintextParser.from_string(text, tokenizer)
summarizer = TextRankSummarizer(stemmer)

summary = summarizer(parser.document, sentence_count)

if os.path.isfile(summary_txt):
    os.remove(summary_txt)
with open(summary_txt, "a") as f:
    for sentence in summary:
        f.write(textwrap.fill(str(sentence), 79) + "\n\n")
